#include "../networkConfig.h"

#define UPLOAD 1
#define DOWNLOAD 2

struct sockaddr_in servAddr, heartbeatAddr, broadcastAddr, cliAddr, peerAddr;
int cliSd;
clock_t timeBegin, timeMeasure;
int broadcastPort, serverHeartbeatPort, cliPort;
char serverPort[5] = "\0\0\0\0\0", peerPort[5] = "\0\0\0\0\0", betaPort[5] = "\0\0\0\0\0", fileName[35] = "\0", command[40], peerFileName[35] = "\0";
char servBuf[1025], heartbeatBuf[5], broadcastBuf[40], cliBuf[1025], modified[40];
int isHeartBeating = 0, awaitPeerFile = 0, peerFileReady = 0, isCommandReady = 0, commandStat = 0, awaitServerFile = 0, serverFileReceived = 0;
int fileNotFound = 0, havePeerFile = 0, stopAskingPeers = 0, sendingToPeer = 0, isCliSdPresent = 0, uselessServer = 0, heartbeatTester = 0;
int serverUploadStatus = 0, isRebroadcastTime = 1;

void initBuffer(char* buf)
{
	for (int i = 0; i < strlen(buf); i++)
		buf[i] = '\0';
}

void reverse(char str[], int length)
{
	int start = 0;
	int end = length -1;
	while (start < end)
	{
		int temp = str[start];
		str[start] = str[end];
		str[end] = str[start];
		start++;
		end--;
	}
}

char* itoa(int num)
{
	int base = 10;
	int i = 0;
	int isNegative = 0;

	if (num == 0)
	{
		modified[i++] = '0';
		modified[i] = '\0';
		return modified;
	}

	if (num < 0 && base == 10)
	{
		isNegative = 1;
		num = -num;
	}

	while (num != 0)
	{
		int rem = num % base;
		modified[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
		num = num/base;
	}

	if (isNegative)
	modified[i++] = '-';
	modified[i] = '\0';
	reverse(modified, i);
	return modified;
}

char* setPeerMessage(char* fileName, const char* port, char* modified)
{
	modified[0] = port[0]; modified[1] = port[1]; modified[2] = port[2]; modified[3] = port[3]; modified[4] = ' ';
	for (int i = 5; i < strlen(fileName) + 5; i++)
		modified[i] = fileName[i - 5];
	modified[strlen(fileName) + 5] = '\0';
	return modified;
}

char* getPeerMessage(char* message, char* modified)
{
	for (int i = 5; i < strlen(message); i++)
		modified[i - 5] = message[i];
	modified[strlen(message) - 5] = '\0';
	return modified;
}

int equalStrs(char* st1, char* st2, int subInd)
{
	if (subInd > strlen(st1) || subInd > strlen(st2))
		return 0;
	for (int i = 0; i < subInd; i++)
		if (st1[i] != st2[i])
			return 0;
	return 1;
}

void setFileName(char* cmd, int subInd)
{
	for (int i = subInd; i < strlen(cmd); i++)
		fileName[i - subInd] = cmd[i];
	fileName[strlen(cmd) - subInd] = '\0';
}

int parser(char* cmd)
{
	if (strlen(cmd) >= 8 && equalStrs(cmd, "upload ", 7)) // upload 1
	{
		setFileName(cmd, 7);
		return UPLOAD;
	}
	else if (strlen(cmd) >= 10 && equalStrs(cmd, "download ", 9)) // download 2
	{
		setFileName(cmd, 9);
		return DOWNLOAD;
	}
	else
	{
		return -1;
	}

	return 0;
}

int incomingConnection(int servSock, int heartbeatSock, int broadcastSock, int cliSock)
{
	int ret, cliAddrLen = sizeof(cliAddr);
	struct timeval tv;
	fd_set fds;
	int max_sd;
	max_sd = cliSock > broadcastSock ? cliSock : broadcastSock;
	max_sd = heartbeatSock > max_sd ? heartbeatSock : max_sd;
	max_sd = servSock > max_sd ? servSock : max_sd;
	max_sd = STDIN_FILENO > max_sd ? STDIN_FILENO : max_sd;
	if (isCliSdPresent)
		max_sd = cliSd > max_sd ? cliSd : max_sd;

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	FD_ZERO(&fds);

	FD_SET(STDIN_FILENO, &fds);
	FD_SET(servSock, &fds);
	FD_SET(heartbeatSock, &fds);
	FD_SET(broadcastSock, &fds);
	FD_SET(cliSock, &fds);
	if (isCliSdPresent)
		FD_SET(cliSd, &fds);

	select(max_sd+1, &fds, NULL, NULL, &tv);

	if (FD_ISSET(STDIN_FILENO, &fds))	// user commands
	{
		ret = read(STDIN_FILENO, (char *)command, sizeof(command));
		command[ret - 1] = '\0';
		if(ret > 0)
		{
			write(1, "Command received from user: ", strlen("Command received from user: "));
			write(1, command, strlen(command));
			write(1, "\n", strlen("\n"));
			isCommandReady = 1;
			if (heartbeatTester == 0 && isHeartBeating == 1)
			{
				isHeartBeating = 0;
				write(1, "Heartbeat has stopped! Server is considered unavailable!\n",
					strlen("Heartbeat has stopped! Server is considered unavailable!\n"));
			}
			heartbeatTester = 0;
		}
	}
	if (FD_ISSET(servSock, &fds))	// server messages
	{
		ret = recv(servSock,(char *)servBuf,sizeof(servBuf), 0);
		servBuf[ret] = '\0';
		if(ret > 0 )
		{
			write(1, "Message received from server: ", strlen("Message received from server: "));
			write(1, servBuf, strlen(servBuf));
			write(1, "\n", strlen("\n"));
			if (equalStrs(servBuf, "Filename accepted!", 18) && serverUploadStatus == 2)
				serverUploadStatus = 3;
			if (awaitServerFile == 1)
			{
				awaitServerFile = 0;
				if (servBuf[0] != '-')
					serverFileReceived = 1;
				else if (equalStrs(servBuf, "-Server: File not found!", 23))
				{
					fileNotFound = 1;
					uselessServer = 1;
				}
			}

		}
	}
	if (FD_ISSET(heartbeatSock, &fds))	// heartbeats
	{
		ret = recv(heartbeatSock,(char *)heartbeatBuf,sizeof(heartbeatBuf), 0);
		heartbeatBuf[ret] = '\0';
		if(ret > 0 )
		{
			if(isHeartBeating == 0)
			{
				isHeartBeating = 1;
				serverPort[0] = heartbeatBuf[0]; serverPort[1] = heartbeatBuf[1]; serverPort[2] = heartbeatBuf[2]; serverPort[3] = heartbeatBuf[3];
				serverPort[4] = '\0';
				write(1, "Server is running on port ", strlen("Server is running on port "));
				write(1, serverPort, strlen(serverPort));
				write(1, "\n", strlen("\n"));
				servAddr.sin_port = htons(atoi(serverPort));
				if(connect(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
					write(1, "server connect() failed!\n", strlen("server connect() failed!\n"));
			}
		}
	}
	if (FD_ISSET(broadcastSock, &fds))	// broadcast channel
	{
		ret = recv(broadcastSock,(char *)broadcastBuf,sizeof(broadcastBuf), 0);
		broadcastBuf[ret] = '\0';
		if(ret > 0 )
		{
			if (ret < 6)
				write(1, "Corrupt message on broadcast!\n", strlen("Corrupt message on broadcast!\n"));
			else
			{
				int desc;
				betaPort[0] = broadcastBuf[0]; betaPort[1] = broadcastBuf[1]; betaPort[2] = broadcastBuf[2]; betaPort[3] = broadcastBuf[3];
				betaPort[4] = '\0';
				if (atoi(betaPort) != cliPort)
				{
					peerPort[0] = betaPort[0]; peerPort[1] = betaPort[1]; peerPort[2] = betaPort[2]; peerPort[3] = betaPort[3]; peerPort[4] = '\0';
					write(1, "Message received from peers: ", strlen("Message received from peers: "));
					write(1, broadcastBuf, strlen(broadcastBuf));
					write(1, "\n", strlen("\n"));
					getPeerMessage(broadcastBuf, peerFileName);
					if ((desc = open(peerFileName, O_RDWR)) >= 0)
					{
						if (sendingToPeer == 0)
						{
							write(1, "Peer has requested a file available here!\n", strlen("Peer has requested a file available here!\n"));
							sendingToPeer = 1;
						}
						havePeerFile = 1;
						peerAddr.sin_port = htons(atoi(peerPort));	/* Peer port */
						close(desc);
					}
				}
			}
		}
	}
	if (FD_ISSET(cliSock, &fds) && (cliSd = accept(cliSock, (struct sockaddr *) &cliAddr, (socklen_t*) &cliAddrLen)) >= 0)	// client files
	{
		isCliSdPresent = 1;

		write(1, "New connection, Socket fd is ", strlen("New connection, Socket fd is "));
		write(1, itoa(cliSd), strlen(itoa(cliSd)));
		write(1, ", IP is: ", strlen(", IP is: "));
		write(1, inet_ntoa(cliAddr.sin_addr), strlen(inet_ntoa(cliAddr.sin_addr)));
		write(1, ", port: ", strlen(", port: "));
		write(1, itoa(ntohs(cliAddr.sin_port)), strlen(itoa(ntohs(cliAddr.sin_port))));
		write(1, "\n", strlen("\n"));

		ret = recv(cliSd,(char *)cliBuf,sizeof(cliBuf), MSG_DONTWAIT);
		cliBuf[ret] = '\0';
		write(1, "Client is being contacted!\n", strlen("Client is being contacted!\n"));
		if (ret == 0)
		{
			//Somebody disconnected , get his details and print
			getpeername(cliSd, (struct sockaddr*) &cliAddr, (socklen_t*) &cliAddrLen);
			
			write(1, "Peer disconnected, IP ", strlen("Peer disconnected, IP "));
			write(1, inet_ntoa(cliAddr.sin_addr), strlen(inet_ntoa(cliAddr.sin_addr)));
			write(1, ", port ", strlen(", port "));
			write(1, itoa(ntohs(cliAddr.sin_port)), strlen(itoa(ntohs(cliAddr.sin_port))));
			write(1, "\n", strlen("\n"));
				 
			//Close the socket and mark as 0 in list for reuse
			close(cliSd);
			isCliSdPresent = 0;
		}
		else
		{
			write(1, "File received from a peer: ", strlen("File received from a peer: "));
			write(1, cliBuf, strlen(cliBuf));
			write(1, "\n", strlen("\n"));
			peerFileReady = 1;
			stopAskingPeers = 1;
		}
	}

	return ret ;
}

char* upDownSign(char* fileName, char* modified, int commandStat)
{
	if (commandStat == 1)
		modified[0] = '!';
	else if (commandStat == 2)
		modified[0] = '?';
	for (int i = 1; i <= strlen(fileName); i++)
		modified[i] = fileName[i-1];
	modified[strlen(fileName) + 1] = '\0';
	return modified;
}

int main(int argc, char const *argv[])
{
	int servSock, heartbeatSock, broadcastSock, fileDesc, peerSock, peerFileDesc, cliSock;
	int commandLen, recvMsgSize;
	char dataBuffer[1025];
	int parseRes;
	double timeVal;

	int opt = 1;

	initBuffer(dataBuffer); initBuffer(modified); initBuffer(fileName); initBuffer(command);

	if (argc != 4)
	{
		write(1, "Not started properly!\n", strlen("Not started properly!\n"));
		return 0;
	}

	serverHeartbeatPort = atoi(argv[1]);
	broadcastPort = atoi(argv[2]);
	cliPort = atoi(argv[3]);

	/* Create reliable, stream sockets using TCP */
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		write(1, "Client socket() failed!\n", strlen("Client socket() failed!\n"));
		return 0;
	}

	if ((heartbeatSock = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
	{
		write(1, "heartbeat socket() failed\n", strlen("heartbeat socket() failed\n"));
		return 0;
	}

	if ((broadcastSock = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
	{
		write(1, "broadcast socket() failed\n", strlen("broadcast socket() failed\n"));
		return 0;
	}

	if ((cliSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		write(1, "client socket() failed\n", strlen("client socket() failed\n"));
		return 0;
	}

	opt = 1;
	if(setsockopt(servSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0 ||
		setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0)
	{
		write(1, "server setsockopt() failed\n", strlen("server setsockopt() failed\n"));
		return 0;
	}

	opt = 1;
	if(setsockopt(heartbeatSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0 ||
		setsockopt(heartbeatSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0 ||
		setsockopt(heartbeatSock, SOL_SOCKET, SO_BROADCAST, &opt, sizeof opt) < 0)
	{
		write(1, "heartbeat setsockopt() failed\n", strlen("heartbeat setsockopt() failed\n"));
		return 0;
	}

	opt = 1;
	if(setsockopt(broadcastSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0 ||
		setsockopt(broadcastSock, SOL_SOCKET, SO_BROADCAST, &opt, sizeof opt) < 0 ||
		setsockopt(broadcastSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0)
	{
		write(1, "broadcast setsockopt() failed\n", strlen("broadcast setsockopt() failed\n"));
		return 0;
	}

	opt = 1;
	if(setsockopt(cliSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0 ||
		setsockopt(cliSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0)
	{
		write(1, "client setsockopt() failed\n", strlen("client setsockopt() failed\n"));
		return 0;
	}

	servAddr.sin_family = AF_INET;						/* Internet address family */
	servAddr.sin_addr.s_addr = inet_addr(SERVER_IP);	/* Server IP address*/

	heartbeatAddr.sin_family = AF_INET;
	heartbeatAddr.sin_addr.s_addr = INADDR_BROADCAST;
	heartbeatAddr.sin_port = htons(serverHeartbeatPort);	/* Server heartbeat port */

	broadcastAddr.sin_family = AF_INET;
	broadcastAddr.sin_addr.s_addr = INADDR_BROADCAST;
	broadcastAddr.sin_port = htons(broadcastPort);	/* Broadcast port */

	cliAddr.sin_family = AF_INET;
	cliAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	cliAddr.sin_port = htons(cliPort);	/* Client port */

	peerAddr.sin_family = AF_INET;
	peerAddr.sin_addr.s_addr = inet_addr(SERVER_IP);

	if (bind(heartbeatSock, (struct sockaddr *) &heartbeatAddr, sizeof(heartbeatAddr)) < 0)
	{
		write(1, "heartbeat bind() failed! Perhaps the listening port is busy. Try changing it.\n",
			strlen("heartbeat bind() failed! Perhaps the listening port is busy. Try changing it.\n"));
		return 0;
	}

	if (bind(broadcastSock, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
	{
		write(1, "broadcast bind() failed! Perhaps the listening port is busy. Try changing it.\n",
			strlen("broadcast bind() failed! Perhaps the listening port is busy. Try changing it.\n"));
		return 0;
	}

	if (bind(cliSock, (struct sockaddr *) &cliAddr, sizeof(cliAddr)) < 0)
	{
		write(1, "client bind() failed! Perhaps the listening port is busy. Try changing it.\n",
			strlen("client bind() failed! Perhaps the listening port is busy. Try changing it.\n"));
		return 0;
	}

	/* Mark the socket so it will listen for incoming connections */
	if (listen(cliSock, 2) < 0)
	{
		write(1, "client listen() failed!\n", strlen("client listen() failed!\n"));
		return 0;
	}

	write(1, "Entering commandline:\n", strlen("Entering commandline:\n"));

	for(;;)
	{
		isCommandReady = 0;

		incomingConnection(servSock, heartbeatSock, broadcastSock, cliSock);

		if (isCommandReady)		// Recognizing command and setting the required flags
			if ((commandStat = parser(command)) < 0)
			{
				isCommandReady = 0;
				write(1, "Command was not parsed!\n", strlen("Command was not parsed!\n"));
			}
			else if (commandStat == UPLOAD)
			{
				serverUploadStatus = 1;
			}
			else if (commandStat == DOWNLOAD)
			{
				awaitPeerFile = 1;
			}

		if(isHeartBeating && (commandStat == UPLOAD || commandStat == DOWNLOAD) && uselessServer == 0)
		{
			if (commandStat == 1) // upload
			{
				if (serverUploadStatus == 1) // send filename
				{
					if (send(servSock, upDownSign(fileName, modified, commandStat), strlen(fileName) + 1, 0) != strlen(fileName) + 1)
					{
						write(1, "send() sent a different number of bytes than expected!\n",
							strlen("send() sent a different number of bytes than expected!\n"));
						continue;
					}

					serverUploadStatus = 2;
				}
				else if (serverUploadStatus == 2) // await server's acceptance
				{
					continue;
				}
				else if (serverUploadStatus == 3) // send file
				{
					int readLen;
					fileDesc = open(fileName, O_RDWR);
					readLen = read(fileDesc, dataBuffer, 1024);
					if(send(servSock, dataBuffer, readLen, 0) < readLen)
						write(1, "Upload was not complete!\n", strlen("Upload was not complete!\n"));
					close(fileDesc);
					initBuffer(dataBuffer);
					serverUploadStatus = 0;
					commandStat = 0;
					isCommandReady = 0;
				}
				
			}
			else if (commandStat == 2 || serverFileReceived == 1) // download
			{
				if (awaitServerFile == 0 && serverFileReceived == 0)
				{
					if (send(servSock, upDownSign(fileName, modified, commandStat), strlen(fileName) + 1, 0) != strlen(fileName) + 1)
					{
						write(1, "send() sent a different number of bytes than expected!\n",
							strlen("send() sent a different number of bytes than expected!\n"));
						continue;
					}
					else
						awaitServerFile = 1;

				}
				else if (serverFileReceived == 1)
				{
					write(1, dataBuffer, strlen(dataBuffer));
					write(1, "\n", strlen("\n"));
					fileDesc = open(fileName, O_CREAT | O_RDWR, 0777);
					if (write(fileDesc, servBuf, strlen(servBuf)) != strlen(servBuf))
						write(1, "Download write was not completed!\n", strlen("Download write was not completed!\n"));
					else
						write(1, "Download write was completed!\n", strlen("Download write was completed!\n"));
					close(fileDesc);
					commandStat = 0;
					isCommandReady = 0;
					serverFileReceived = 0;
					awaitServerFile = 0;
				}

			}
		}
		if((isHeartBeating == 0 || fileNotFound) && (commandStat == DOWNLOAD || awaitPeerFile == 1) && isCliSdPresent == 0)
		{
			char* peerMessage = setPeerMessage(fileName, argv[3], modified);

			if(fileNotFound)
			{
				write(1, "File \"", strlen("File \""));
				write(1, fileName, strlen(fileName));
				write(1, " not found on the server!\n", strlen(" not found on the server!\n"));
			}
			
			timeMeasure = clock();
			timeVal = (double)(timeMeasure - timeBegin) / CLOCKS_PER_SEC;
			if (stopAskingPeers == 0 && timeVal > 10)
			if (sendto(broadcastSock, peerMessage, strlen(peerMessage), 0,
				(const struct sockaddr*) &broadcastAddr, sizeof broadcastAddr) != strlen(peerMessage))
			{
				write(1, "Broadcast unsuccessful!\n", strlen("Broadcast unsuccessful!\n"));
				awaitPeerFile = 0;
				continue;
			}
			else
			{
				write(1, "Request broadcasted! ", strlen("Request broadcasted! "));
				write(1, itoa(cliPort), strlen(itoa(cliPort)));
				write(1, "\n", strlen("\n"));
				timeBegin = clock();
			}

			fileNotFound = 0;
			commandStat = 0;
			
			// sleep(1); // Recommended for better performance
		}
		if (awaitPeerFile == 1 && peerFileReady == 1)
		{
			write(1, "Peer file received!\n", strlen("Peer file received!\n"));
			fileDesc = open(fileName, O_CREAT | O_RDWR, 0777);
			if (write(fileDesc, cliBuf, strlen(cliBuf)) != strlen(cliBuf))
				write(1, "Download was not completed!\n", strlen("Download was not completed!\n"));
			else
				write(1, "Download was completed!\n", strlen("Download was completed!\n"));
			close(fileDesc);
			close(cliSd);

			isCliSdPresent = 0;
			awaitPeerFile = 0;
			peerFileReady = 0;
			stopAskingPeers = 0;
			uselessServer = 0;
		}
		if(havePeerFile)
		{
			int readLen;

			if ((peerSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
			{
				write(1, "peer socket() failed!\n", strlen("peer socket() failed!\n"));
				return 0;
			}

			opt = 1;
			if(setsockopt(peerSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0 ||
				setsockopt(peerSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0)
			{
				write(1, "peer setsockopt() failed!\n", strlen("peer setsockopt() failed!\n"));
				return 0;
			}

			if(connect(peerSock, (struct sockaddr *) &peerAddr, sizeof(peerAddr)) < 0)
			{
				write(1, "peer connect() failed!\n", strlen("peer connect() failed!\n"));
				havePeerFile = 0;
				sendingToPeer = 1;
				continue;
			}
			else
				write(1, "Connected to peer!\n", strlen("Connected to peer!\n"));

			peerFileDesc = open(peerFileName, O_RDWR);
			readLen = read(peerFileDesc, dataBuffer, 1024);
			dataBuffer[readLen] = '\0';
			if(send(peerSock, dataBuffer, readLen, 0) < readLen)
				write(1, "Peer file send was not complete!\n", strlen("Peer file send was not complete!\n"));
			else
			{
				write(1, "Peer file is sent! ", strlen("Peer file is sent! "));
				write(1, dataBuffer, strlen(dataBuffer));
				write(1, "\n", strlen("\n"));
			}
			close(peerFileDesc);
			close(peerSock);
			havePeerFile = 0;
			sendingToPeer = 0;
		}

	}

	return 0;
}