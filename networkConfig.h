#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>	//FD_SET, FD_ISSET, FD_ZERO macros
#include <time.h>
#include <signal.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef BROADCAST_PORT
#define BROADCAST_PORT 5500
#endif

#ifndef SERVER_LISTEN_PORT
#define SERVER_LISTEN_PORT "5605"
#endif

#ifndef SERVER_IP
#define SERVER_IP "127.0.0.1"
#endif

#ifndef MAX_CLIENTS
#define MAX_CLIENTS 30
#endif