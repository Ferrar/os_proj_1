#include "../networkConfig.h"

#define MAX_PENDING 5
#define BUFFER_SIZE 1025

void reverse(char str[], int length)
{
	int start = 0;
	int end = length -1;
	while (start < end)
	{
		int temp = str[start];
		str[start] = str[end];
		str[end] = str[start];
		start++;
		end--;
	}
}

char modified [40];
char* itoa(int num)
{
	int base = 10;
	int i = 0;
	int isNegative = 0;

	if (num == 0)
	{
		modified[i++] = '0';
		modified[i] = '\0';
		return modified;
	}

	if (num < 0 && base == 10)
	{
		isNegative = 1;
		num = -num;
	}

	while (num != 0)
	{
		int rem = num % base;
		modified[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
		num = num/base;
	}

	if (isNegative)
	modified[i++] = '-';
	modified[i] = '\0';
	reverse(modified, i);
	return modified;
}

char* getFileName(char* message, char* fileName)
{
	for (int i = 0; i < strlen(message) - 1; i++)
		fileName[i] = message[i + 1];
	fileName[strlen(message) - 1] = '\0';
	return fileName;
}

void initBuffer(char* buf)
{
	for (int i = 0; i < strlen(buf); i++)
		buf[i] = '\0';
}

int main(int argc, char const *argv[])
{
	int servSock, newSocket, heartbeatSock, clientSock[MAX_CLIENTS];
	int addrLen, recvMsgSize;
	int heartbeatPort;
	int opt = 1, i, sd, max_sd, activity, valread, fileDesc;
	char dataBuffer[BUFFER_SIZE], downloadBuffer[BUFFER_SIZE], fileName[35], fileNameBuf[40];
	char* welcomeMessage = "+Server: Welcome to the server!\0";
	char* heartbeatMessage = SERVER_LISTEN_PORT;
	struct sockaddr_in servAddr, heartbeatAddr;
	int isHeartBeating = 0, uploadPhase = 0;

	struct timeval timeOut;

	double elapsedTime = 0;
	clock_t tic, toc;

	initBuffer(dataBuffer); initBuffer(fileName);

	void heartbeater(int signum)
	{
		// Send heartbeat
		if(sendto(heartbeatSock, heartbeatMessage, strlen(heartbeatMessage), /*MSG_CONFIRM*/0,
		(const struct sockaddr *) &heartbeatAddr, sizeof(heartbeatAddr)) != strlen(heartbeatMessage))
		{
			if (isHeartBeating)
				isHeartBeating = 0;
			write(1, "Server's heart is dead!\n", strlen("Server's heart is dead!\n"));
			return;
		}
		else
			if (isHeartBeating == 0)
			{
				isHeartBeating = 1;
				write(1, "Server's heart is beating!\n", strlen("Server's heart is beating!\n"));
			}
		alarm(1);
	}

	if (argc != 2)
	{
		write(1, "Not started properly!\n", strlen("Not started properly!\n"));
		return 0;
	}

	heartbeatPort = atoi(argv[1]);
	write(1, "The heartbeat port: ", strlen("The heartbeat port: "));
	write(1, itoa(heartbeatPort), strlen(itoa(heartbeatPort)));
	write(1, "\n", strlen("\n"));

	//set of socket descriptors
	fd_set readfds;

	//initialise all clientSock[] to 0 so not checked
	for (i = 0; i < MAX_CLIENTS; i++)
		clientSock[i] = 0;

	/* Create socket for incoming connections */
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		write(1, "server socket() failed!\n", strlen("server socket() failed!\n"));
		return 0;
	}

	// Trying to make servSock non-blocking
	fcntl(servSock, F_SETFL, O_NONBLOCK);

	/* Create socket for heartbeat */
	if ((heartbeatSock = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
	{
		write(1, "heartbeat socket() failed!\n", strlen("heartbeat socket() failed!\n"));
		return 0;
	}

	// set master socket to allow multiple connections
	// this is just a good habit, it will work without this
	opt = 1;
	if(setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ||
		setsockopt(servSock, SOL_SOCKET, SO_REUSEPORT, (char *)&opt, sizeof(opt)) < 0)
	{
		write(1, "server setsockopt() failed!\n", strlen("server setsockopt() failed!\n"));
		return 0;
	}

	opt = 1;
	if(setsockopt(heartbeatSock, SOL_SOCKET, SO_BROADCAST, &opt, sizeof opt) < 0 ||
		setsockopt(heartbeatSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0 ||
		setsockopt(heartbeatSock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof opt) < 0)
	{
		write(1, "heartbeat setsockopt() failed!\n", strlen("heartbeat setsockopt() failed!\n"));
		return 0;
	}
	
	


	servAddr.sin_family = AF_INET;						/* Internet servAddr family */
	servAddr.sin_addr.s_addr = inet_addr(SERVER_IP);		/* Any incoming interface */
	servAddr.sin_port = htons(atoi(SERVER_LISTEN_PORT));					/* Local port */

	heartbeatAddr.sin_family = AF_INET;						/* Internet servAddr family */
	heartbeatAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);		/* Any incoming interface */
	heartbeatAddr.sin_port = htons(heartbeatPort);					/* Heartbeat port */
	
	if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
	{
		write(1, "server bind() failed! Perhaps the listening port is busy. Try changing it.\n",
			strlen("server bind() failed! Perhaps the listening port is busy. Try changing it.\n"));
		return 0;
	}

	if (bind(heartbeatSock, (struct sockaddr *) &heartbeatAddr, sizeof(heartbeatAddr)) < 0)
	{
		write(1, "heartbeat bind() failed! Perhaps the listening port is busy. Try changing it.\n",
			strlen("heartbeat bind() failed! Perhaps the listening port is busy. Try changing it.\n"));
		return 0;
	}

	//set up heartbeater as alarm handler
	signal(SIGALRM, heartbeater);

	//schedule alarm for 1 second
	alarm(1);

	//do not proceed until signal is handled
	// pause();

	write(1, "Listening on port: ", strlen("Listening on port: "));
	write(1, SERVER_LISTEN_PORT, strlen(SERVER_LISTEN_PORT));
	write(1, "\n", strlen("\n"));

	/* Mark the socket so it will listen for incoming connections */
	if (listen(servSock, MAX_PENDING) < 0)
	{
		write(1, "listen() failed!\n", strlen("listen() failed!\n"));
		return 0;
	}

	//accepting incoming connections
	addrLen = sizeof(servAddr);
	write(1, "Waiting for connections ...\n", strlen("Waiting for connections ...\n"));

	for (;;) /* Run forever */
	{
		// clear the socket set
		FD_ZERO(&readfds);

		//add master socket to set
		FD_SET(servSock, &readfds);
		max_sd = servSock;
		
		//add child sockets to set
		for ( i = 0 ; i < MAX_CLIENTS ; i++)
		{
			//socket descriptor
			sd = clientSock[i];

			//if socket descriptor is valid then add to read list
			if(sd > 0)
				FD_SET(sd ,&readfds);

			//highest file descriptor number, need it for the select function
			if(sd > max_sd)
				max_sd = sd;
		}

		//wait for an activity on one of the sockets, timeout is NULL, so wait indefinitely
		activity = select( max_sd + 1 , &readfds , NULL , NULL , &timeOut);

		if ((activity < 0) && (errno!=EINTR))
			write(1, "select() failed!\n", strlen("select() failed!\n"));

		//If something happened on the master socket, then its an incoming connection
		if (FD_ISSET(servSock, &readfds) && (newSocket = accept(servSock, (struct sockaddr *) &servAddr, (socklen_t*) &addrLen)) >= 0)
		{
			// if ((newSocket = accept(servSock, (struct sockaddr *) &servAddr, (socklen_t*) &addrLen)) < 0)
			// {
				// write(1, "accept() failed!\n");
				// return 0;
			// }
			 
			//inform user of socket number - used in send and receive commands
			// write(1, "New connection, Socket fd is %d, IP is: %s, port: %d\n", newSocket, inet_ntoa(servAddr.sin_addr), ntohs(servAddr.sin_port));
			write(1, "New connection, Socket fd is ", strlen("New connection, Socket fd is "));
			write(1, itoa(newSocket), strlen(itoa(newSocket)));
			write(1, ", IP is: ", strlen(", IP is: "));
			write(1, inet_ntoa(servAddr.sin_addr), strlen(inet_ntoa(servAddr.sin_addr)));
			write(1, ", port: ", strlen(", port: "));
			write(1, itoa(ntohs(servAddr.sin_port)), strlen(itoa(ntohs(servAddr.sin_port))));
			write(1, "\n", strlen("\n"));

			//send new connection greeting message
			if(send(newSocket, welcomeMessage, strlen(welcomeMessage), 0) != strlen(welcomeMessage))
				write(1, "Welcome message send() failed!\n", strlen("Welcome message send() failed!\n"));
				 
			write(1, "Welcome message sent successfully!\n", strlen("Welcome message sent successfully!\n"));
				 
			//add new socket to array of sockets
			for (i = 0; i < MAX_CLIENTS; i++)
			{
				//if position is empty
				if(clientSock[i] == 0)
				{
					clientSock[i] = newSocket;
					// write(1, "Adding to list of sockets as %d\n", i);
					write(1, "Adding to list of sockets as ", strlen("Adding to list of sockets as "));
					write(1, itoa(i), strlen(itoa(i)));
					write(1, "\n", strlen("\n"));
					break;
				}
			}
		}
		//else its some IO operation on some other socket 
		for (i = 0; i < MAX_CLIENTS; i++)
		{
			sd = clientSock[i];
				 
			if (FD_ISSET(sd, &readfds))
			{
				//Check if it was for closing, and also read the incoming message
				if (uploadPhase == 0)
					valread = recv(sd, fileNameBuf, 39, MSG_DONTWAIT);
				else
					valread = recv(sd, dataBuffer, 1024, MSG_DONTWAIT);

				if (valread == 0)
				{
					//Somebody disconnected , get his details and print
					getpeername(sd, (struct sockaddr*) &servAddr, (socklen_t*) &addrLen);
					// write(1, "Host disconnected, IP %s, port %d\n", inet_ntoa(servAddr.sin_addr) , ntohs(servAddr.sin_port));
					write(1, "Host disconnected, IP ", strlen("Host disconnected, IP "));
					write(1, inet_ntoa(servAddr.sin_addr), strlen(inet_ntoa(servAddr.sin_addr)));
					write(1, ", port ", strlen(", port "));
					write(1, itoa(ntohs(servAddr.sin_port)), strlen(itoa(ntohs(servAddr.sin_port))));
					write(1, "\n", strlen("\n"));
						 
					//Close the socket and mark as 0 in list for reuse
					close(sd);
					clientSock[i] = 0;
				}
					 
				//Process the message that came in
				else 
				{
					//set the string terminating NULL byte on the end of the data read
					if (uploadPhase == 0)
					{
						fileNameBuf[valread] = '\0';
						getFileName(fileNameBuf, fileName);					
					}
					else
						dataBuffer[valread] = '\0';

					if (fileNameBuf[0] == '!' || uploadPhase == 1)
					{
						if (uploadPhase == 0)
						{
							// write(1, "The file \"%s\" is to be uploaded to the server!\n", fileName);
							write(1, "The file \"", strlen("The file \""));
							write(1, fileName, strlen(fileName));
							write(1, "\" is to be uploaded to the server!\n", strlen("\" is to be uploaded to the server!\n"));
							send(sd, "Filename accepted!\0", 19, 0);
							uploadPhase = 1;
						}
						else if (uploadPhase == 1)
						{
							fileDesc = open(fileName, O_CREAT | O_RDWR, 0777);
							if (write(fileDesc, dataBuffer, strlen(dataBuffer)) != strlen(dataBuffer))
							{
								write(1, "Upload was not completed!\n", strlen("Upload was not completed!\n"));
								send(sd, "File not accepted!\0", 19, 0);
							}
							else
							{
								write(1, "Upload is complete!\n", strlen("Upload is complete!\n"));
								send(sd, "File accepted!\0", 15, 0);
							}
							close(fileDesc);
							fileNameBuf[0] = '\0';
							uploadPhase = 0;
						}
					}
					else if(fileNameBuf[0] == '?')
					{
						// write(1, "The file \"%s\" is to be downloaded from the server!\n", fileName);
						write(1, "The file \"", strlen("The file \""));
						write(1, fileName, strlen(fileName));
						write(1, "\" is to be downloaded to the server!\n", strlen("\" is to be downloaded to the server!\n"));
						if ((fileDesc = open(fileName, O_RDWR)) < 0)
						{
							write(1, "File not found!\n", strlen("File not found!\n"));
							send(sd, "-Server: File not found!\0", 25, 0);
						}
						else
						{
							int readLen;
							readLen = read(fileDesc, downloadBuffer, 1024);
							downloadBuffer[readLen] = '\0';
							if(send(sd, downloadBuffer, readLen + 1, 0) < readLen)
							{
								write(1, "Download was not complete!\n", strlen("Download was not complete!\n"));
								send(sd, "-Server: Download was not completed! This is mostly because the file does not exist.\0", 85, 0);
							}
							else
							{
								write(1, "Download is completed!\n", strlen("Download is completed!\n"));
								// send(sd, "\n+Server: Download is completed!\0", 33, 0);
							}
							close(fileDesc);
						}
						fileNameBuf[0] = '\0';
					}
				}
			}
		}
	}
		 
	return 0;
}
